﻿using System.Runtime.InteropServices;

namespace CCLauncherConfirmer.Structs
{
    // http://pinvoke.net/default.aspx/Structures/INPUT.html
    [StructLayout(LayoutKind.Sequential)]
    struct INPUT
    {
        internal uint type;
        internal InputUnion U;
        internal static int Size
        {
            get { return Marshal.SizeOf(typeof(INPUT)); }
        }
    }
}
