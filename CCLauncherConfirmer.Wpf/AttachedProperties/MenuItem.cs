﻿using System.Windows;

namespace CCLauncherConfirmer.Wpf.AttachedProperties
{
    class MenuItem
    {
        #region Static Dependency Properties
        public static readonly DependencyProperty ContainerMinWidthProperty;
        public static readonly DependencyProperty ContainerWidthProperty;
        public static readonly DependencyProperty IconScaleProperty;
        public static readonly DependencyProperty SpaceWidthProperty;
        #endregion

        #region Static Constructors
        static MenuItem()
        {
            ContainerMinWidthProperty = DependencyProperty.RegisterAttached(
                "ContainerMinWidth",
                typeof(double),
                typeof(MenuItem),
                new PropertyMetadata(0.0)
            );
            ContainerWidthProperty = DependencyProperty.RegisterAttached(
                "ContainerWidth",
                typeof(double),
                typeof(MenuItem),
                new PropertyMetadata(0.0)
            );
            IconScaleProperty = DependencyProperty.RegisterAttached(
                "IconScale",
                typeof(double),
                typeof(MenuItem),
                new PropertyMetadata(1.0)
            );
            SpaceWidthProperty = DependencyProperty.RegisterAttached(
                "SpaceWidth",
                typeof(double),
                typeof(MenuItem),
                new PropertyMetadata(0.0)
            );
        }
        #endregion

        #region Static Public
        public static void SetContainerMinWidth(System.Windows.Controls.MenuItem menuItem, double value)
            => menuItem.SetValue(ContainerMinWidthProperty, value);
        
        public static double GetContainerMinWidth(System.Windows.Controls.MenuItem menuItem)
            => (double)menuItem.GetValue(ContainerMinWidthProperty);

        public static void SetContainerWidth(System.Windows.Controls.MenuItem menuItem, double value)
            => menuItem.SetValue(ContainerWidthProperty, value);

        public static double GetContainerWidth(System.Windows.Controls.MenuItem menuItem)
            => (double)menuItem.GetValue(ContainerWidthProperty);

        public static void SetIconScale(System.Windows.Controls.MenuItem menuItem, double value)
            => menuItem.SetValue(IconScaleProperty, value);

        public static double GetIconScale(System.Windows.Controls.MenuItem menuItem)
            => (double)menuItem.GetValue(IconScaleProperty);

        public static void SetSpaceWidth(System.Windows.Controls.MenuItem menuItem, double value)
            => menuItem.SetValue(SpaceWidthProperty, value);

        public static double GetSpaceWidth(System.Windows.Controls.MenuItem menuItem)
            => (double)menuItem.GetValue(SpaceWidthProperty);
        #endregion
    }
}
