﻿using Saxoier.Utils;
using Saxoier.Utils.Wpf;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Input;

namespace CCLauncherConfirmer.Wpf.ViewModels
{
    class SettingsViewModel : ViewModel
    {
        #region Properties
        public ObservableCollection<Process> Processes { get; }
        public Process SelectedProcess
        {
            get => GetValue<Process>();
            set => SetValue(value);
        }
        public ObservableCollection<ProcessWindow> ProcessWindows { get; }
        public ProcessWindow SelectedProcessWindow
        {
            get => GetValue<ProcessWindow>();
            set => SetValue(value);
        }
        public Image Image
        {
            get => GetValue<Image>();
            private set => SetValue(value);
        }
        public Rectangle BoundingRect
        {
            get => GetValue<Rectangle>();
            private set => SetValue(value);
        }
        #endregion

        #region Constructor
        public SettingsViewModel()
        {
            DispatchClickEventCommand = new RelayCommand(DispatchClickEvent);
            RefreshProcessesCommand = new RelayCommand(RefreshProcesses);
            RefreshProcessWindowsCommand = new RelayCommand(RefreshProcessWindows);
            TakePictureCommand = new RelayCommand(TakePicture);

            Processes = new ObservableCollection<Process>();
            ProcessWindows = new ObservableCollection<ProcessWindow>();

            RefreshProcesses();
            SelectedProcess = Processes.FirstOrDefault(p => p.MainWindowTitle == "CC Launcher 3.0");
            RefreshProcessWindows();
            SelectedProcessWindow = ProcessWindows.FirstOrDefault(w => w.Title == "Anwesenheitskontrolle");
        }
        #endregion

        #region Commands
        public ICommand DispatchClickEventCommand { get; }
        public ICommand RefreshProcessesCommand { get; }
        public ICommand RefreshProcessWindowsCommand { get; }
        public ICommand TakePictureCommand { get; }
        #endregion

        #region Commandactions
        private void DispatchClickEvent()
        {
            var mouseClicker = new MouseClicker();
            mouseClicker.DispatchMouseEvent(BoundingRect.X + 10, BoundingRect.Y + 10);
        }

        private void RefreshProcesses()
        {
            Processes.Replace(Process.GetProcesses());
            ProcessWindows.Clear();
        }

        private void RefreshProcessWindows()
        {
            if (SelectedProcess == null)
            {
                return;
            }

            ProcessWindows.Replace(ProcessWindow.GetProcessWindows(SelectedProcess.Id));
        }

        private void TakePicture()
        {
            if (SelectedProcessWindow == null)
            {
                Image = null;
                return;
            }

            var hWnd = SelectedProcessWindow.HWnd;
            var sc = new ScreenCapture();
            Image = sc.CaptureWindow(hWnd);
            BoundingRect = sc.GetBoundingRect(hWnd);
        }
        #endregion

        #region Eventraiser
        protected override void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            var p = e.PropertyName;
            if (p == nameof(SelectedProcess))
            {
                RefreshProcessWindows();
            }
            if (p == nameof(SelectedProcessWindow))
            {
                TakePicture();
            }

            base.OnPropertyChanged(e);
        }
        #endregion
    }
}
