﻿using CCLauncherConfirmer.Wpf.Enums;
using System;
using System.Collections;
using System.Globalization;
using System.Windows.Data;

namespace CCLauncherConfirmer.Wpf.Converters
{
    class ComparisonConverter : IMultiValueConverter
    {
        #region Public
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Length != 2)
            {
                return null;
            }

            return (ComparisonResult)Comparer.Default.Compare(values[0], values[1]);
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
