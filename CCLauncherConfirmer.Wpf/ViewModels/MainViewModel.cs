﻿using Saxoier.Utils.Wpf;

namespace CCLauncherConfirmer.Wpf.ViewModels
{
    class MainViewModel : ViewModel
    {
        #region Properties
        public SettingsViewModel SettingsVm{ get; }

        public ViewModel ContentVm
        {
            get => GetValue<ViewModel>();
            private set => SetValue(value);
        }
        #endregion

        #region Constructor
        public MainViewModel()
        {
            SetContentViewModelCommand = new RelayCommand<ViewModel>(SetContentViewModel);

            SettingsVm = new SettingsViewModel();
            SetContentViewModel(SettingsVm);
        }
        #endregion

        #region Commands
        public ICommand<ViewModel> SetContentViewModelCommand { get; }
        #endregion

        #region Commandactions
        private void SetContentViewModel(ViewModel vm)
        {
            ContentVm = vm;
        }
        #endregion
    }
}
