﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Timers;

namespace CCLauncherConfirmer.Cli
{
    class Program
    {
        private static IEnumerable<(Process, ProcessWindow)> GetProcesses()
        {
            foreach (var process in Process.GetProcesses())
            {
                // Skip idle process as it contains every window. Not possible as
                // Process.GetProcesses().Where(p => p.ProcessName != "Idle")
                if (process.ProcessName != "Idle")
                {
                    continue;
                }

                var attendanceCheckWindow = ProcessWindow
                    .GetProcessWindows(process.Id)
                    .FirstOrDefault(w => w.Title == "Anwesenheitskontrolle");
                if (attendanceCheckWindow != null)
                {
                    yield return (process, attendanceCheckWindow);
                }
            }
        }

        private static void PressButton(MouseClicker mouseClicker, Rectangle rect, Rectangle windowRect)
        {
            var x = rect.X + windowRect.X + rect.Width / 2;
            var y = rect.Y + windowRect.Y + rect.Height / 2;
            mouseClicker.DispatchMouseEvent(x, y);
        }

        private static void PressPadButtons(MouseClicker mouseClicker, IEnumerable<string> digits, IDictionary<string, Rectangle> pads, Rectangle windowRect)
        {
            foreach (var digit in digits)
            {
                if (pads.TryGetValue(digit, out Rectangle rect))
                {
                    PressButton(mouseClicker, rect, windowRect);
                }
            }
        }

        private static void Main(string[] args)
        {
            var timer = new Timer();
            timer.Interval = TimeSpan.FromSeconds(2).TotalMilliseconds;
            timer.Elapsed += OnTimerElapsed;
            timer.Start();

            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
            timer.Stop();
        }

        #region Static Eventhandler
        private static void OnTimerElapsed(object sender, EventArgs e)
        {
            var (process, attendanceCheckWindow) = GetProcesses().FirstOrDefault();
            if (process == null)
            {
                return;
            }

            var imageTester = new ImageTester();
            var sc = new ScreenCapture();
            using (var windowImage = sc.CaptureWindow(attendanceCheckWindow.HWnd))
            {
                imageTester.EvaluateRects(windowImage);

                var digits = imageTester
                    .DigitMatches
                    .OrderBy(m => m.match.Rectangle.X)
                    .Select(m => m.name)
                    .ToList();
                // https://stackoverflow.com/a/1704838
                var pads = imageTester
                    .PadDigitMatches
                    .GroupBy(m => m.name)
                    .Select(g => (
                        name: g.Key,
                        rect: g
                            .OrderByDescending(m => m.match.Similarity)
                            .Select(m => m.match.Rectangle)
                            .FirstOrDefault()
                    ))
                    // .Join() how?
                    .ToDictionary(m => m.name, m => m.rect);
                var windowRect = sc.GetBoundingRect(attendanceCheckWindow.HWnd);

                var mouseClicker = new MouseClicker();
                mouseClicker.StoreMousePosition();
                PressPadButtons(mouseClicker, digits, pads, windowRect);
                PressButton(mouseClicker, new Rectangle(276, 383, 75, 25), windowRect); // confirm button
                mouseClicker.RestoreMousePosition();
            }
        }
        #endregion
    }
}
