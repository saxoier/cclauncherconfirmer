﻿using CCLauncherConfirmer.Enums;
using CCLauncherConfirmer.Structs;
using System;
using System.Drawing;
using System.Runtime.InteropServices;

namespace CCLauncherConfirmer
{
    // https://stackoverflow.com/a/2416762
    public class MouseClicker
    {
        #region DllImports
        // http://pinvoke.net/default.aspx/user32/SendInput.html
        /// <summary>
        /// Synthesizes keystrokes, mouse motions, and button clicks.
        /// </summary>
        [DllImport("user32.dll")]
        private static extern uint SendInput(uint nInputs,
           [MarshalAs(UnmanagedType.LPArray), In] INPUT[] pInputs,
           int cbSize);

        [DllImport("user32.dll")]
        private static extern int GetSystemMetrics(SystemMetric smIndex);

        // https://stackoverflow.com/a/5577528
        /// <summary>
        /// Retrieves the cursor's position, in screen coordinates.
        /// </summary>
        /// <see>See MSDN documentation for further information.</see>
        [DllImport("user32.dll")]
        private static extern bool GetCursorPos(out POINT lpPoint);
        #endregion

        #region Properties
        private Point MousePosition { get; set; }
        private Point ScreenPosition
        {
            get => new Point()
            {
                X = GetSystemMetrics(SystemMetric.SM_XVIRTUALSCREEN),
                Y = GetSystemMetrics(SystemMetric.SM_YVIRTUALSCREEN),
            };
        }
        private Size ScreenSize
        {
            get => new Size()
            {
                Width = GetSystemMetrics(SystemMetric.SM_CXVIRTUALSCREEN),
                Height = GetSystemMetrics(SystemMetric.SM_CYVIRTUALSCREEN)
            };
        }
        #endregion

        #region Private
        private Point GetCursorPosition()
        {
            GetCursorPos(out POINT lpPoint);
            return lpPoint;
        }

        private Point ScalePoint(Point position, Size screenSize)
        {
            // -1 from screenSize otherwise off-by-one error (moving mouse cursor)
            // bug if screenSize larger then ushort.MaxValue
            position.X = position.X * ushort.MaxValue / (screenSize.Width - 1);
            position.Y = position.Y * ushort.MaxValue / (screenSize.Height - 1);
            return position;
        }

        private Point AddScreenOffset(Point position, Point screenPosition)
        {
            // screenPosition.X and screenPosition.Y can be negative in a multi
            // monitor setup. Can they be positive too?
            position.X += screenPosition.X * -1;
            position.Y += screenPosition.Y * -1;
            return position;
        }

        private void SendInput(Point position, MOUSEEVENTF flags)
        {
            var input = new[]
            {
                new INPUT
                {
                    type = 0, // INPUT_MOUSE
                    U = new InputUnion
                    {
                        mi = new MOUSEINPUT
                        {
                            dx = position.X,
                            dy = position.Y,
                            mouseData = 0,
                            dwFlags = flags,
                            time = 0,
                            dwExtraInfo = UIntPtr.Zero
                        }
                    }
                }
            };

            SendInput(1, input, INPUT.Size);
        }
        #endregion

        #region Public
        public void StoreMousePosition()
        {
            MousePosition = AddScreenOffset(GetCursorPosition(), ScreenPosition);
        }

        public void RestoreMousePosition()
        {
            var mousePosition = ScalePoint(MousePosition, ScreenSize);
            SendInput(mousePosition, MOUSEEVENTF.ABSOLUTE | MOUSEEVENTF.VIRTUALDESK | MOUSEEVENTF.MOVE);
        }

        public void DispatchMouseEvent(int x, int y)
        {
            var position = AddScreenOffset(new Point(x, y), ScreenPosition);
            var scaledPosition = ScalePoint(position, ScreenSize);
           
            SendInput(scaledPosition, MOUSEEVENTF.ABSOLUTE | MOUSEEVENTF.VIRTUALDESK | MOUSEEVENTF.MOVE);
            SendInput(Point.Empty, MOUSEEVENTF.LEFTDOWN | MOUSEEVENTF.LEFTUP);
        }
        #endregion
    }
}
