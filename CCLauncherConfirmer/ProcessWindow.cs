﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;

namespace CCLauncherConfirmer
{
    // https://stackoverflow.com/a/2584672
    public class ProcessWindow
    {
        #region Constants
        private const uint WM_GETTEXT = 0x000D;
        #endregion

        #region Private Delegates
        private delegate bool EnumThreadDelegate(IntPtr hWnd, IntPtr lParam);
        #endregion

        #region DllImports
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern IntPtr SendMessage(IntPtr hWnd, uint Msg, int wParam, StringBuilder lParam);

        [DllImport("user32.dll")]
        private static extern bool EnumThreadWindows(int dwThreadId, EnumThreadDelegate lpfn, IntPtr lParam);
        #endregion

        #region Static Private
        private static bool AddProcessWindow(ICollection<ProcessWindow> windows, int processId, IntPtr hWnd)
        {
            windows.Add(new ProcessWindow(processId, hWnd));
            return true;
        }
        #endregion

        #region Static Public
        public static IEnumerable<ProcessWindow> GetProcessWindows(int processId)
        {
            var windows = new List<ProcessWindow>();

            foreach (ProcessThread thread in Process.GetProcessById(processId).Threads)
            {
                EnumThreadWindows(thread.Id, (hWnd, lParam) => AddProcessWindow(windows, processId, hWnd), IntPtr.Zero);
            }

            return windows;
        }
        #endregion

        #region Properties
        public int ProcessId { get; private set; }
        public IntPtr HWnd { get; private set; }
        public string Title => GetTitle(HWnd);
        #endregion

        #region Constructor
        private ProcessWindow(int processId, IntPtr hWnd)
        {
            ProcessId = processId;
            HWnd = hWnd;
        }
        #endregion

        #region Private
        public string GetTitle(IntPtr hWnd)
        {
            var message = new StringBuilder(1000);
            SendMessage(hWnd, WM_GETTEXT, message.Capacity, message);
            return message.ToString();
        }
        #endregion
    }
}
