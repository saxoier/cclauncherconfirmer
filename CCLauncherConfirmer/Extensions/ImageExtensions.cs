﻿using System.Drawing;
using System.Drawing.Imaging;

namespace CCLauncherConfirmer.Extensions
{
    static class ImageExtensions
    {
        #region Static Public
        // https://stackoverflow.com/a/26967840
        public static Bitmap ConvertToFormat(this Image image, PixelFormat format)
        {
            var copy = new Bitmap(image.Width, image.Height, format);
            using (var gr = Graphics.FromImage(copy))
            {
                gr.DrawImage(image, new Rectangle(0, 0, copy.Width, copy.Height));
            }
            return copy;
        }
        #endregion
    }
}
