﻿namespace CCLauncherConfirmer.Wpf.Enums
{
    enum ComparisonResult
    {
        IsLess = -1,
        IsEqual = 0,
        IsMore = 1
    }
}
