﻿using Saxoier.Utils.Wpf;
using System.Windows;

namespace CCLauncherConfirmer.Wpf.Fake.ViewModels
{
    class PointViewModel : ViewModel<(int index, Point position)>
    {
        public int Index => Model.index;
        public Point Position => Model.position;
    }
}
