﻿using Saxoier.Utils.Wpf;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace CCLauncherConfirmer.Wpf.Fake.ViewModels
{
    class MainViewModel : ViewModel
    {
        #region Properties
        private Window AttendanceCheckWindow { get; set; }
        private AttendanceCheckViewModel AttendanceCheckVm { get; }
        #endregion

        #region Constructors
        public MainViewModel()
        {
            var attendanceCheckVm = new AttendanceCheckViewModel();

            ClearMouseClicksCommand = attendanceCheckVm.ClearMouseClicksCommand;
            SelectRandomImageCommand = attendanceCheckVm.SelectRandomImageCommand;
            ToggleWindowCommand = new RelayCommand(OpenWindow);

            AttendanceCheckVm = attendanceCheckVm;
        }
        #endregion

        #region Commands
        public ICommand ClearMouseClicksCommand { get; }
        public ICommand SelectRandomImageCommand { get; }
        public ICommand ToggleWindowCommand { get; }
        #endregion

        #region Commandactions
        private void OpenWindow()
        {
            if (AttendanceCheckWindow != null)
            {
                AttendanceCheckWindow.Close();
                AttendanceCheckWindow = null;
                return;
            }

            // TODO: move everything to view
            var window = new Window();
            window.Title = "Anwesenheitskontrolle";
            window.Width = 364;
            window.Height = 421;
            window.WindowStyle = WindowStyle.None;
            window.ResizeMode = ResizeMode.NoResize;
            window.Background = Brushes.Transparent;
            window.Content = AttendanceCheckVm;
            window.Owner = Application.Current.MainWindow;
            window.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            window.MouseMove += (sender, e) =>
            {
                if (e.LeftButton == MouseButtonState.Pressed)
                {
                    Window.GetWindow((DependencyObject)e.Source).DragMove();
                }
            };
            AttendanceCheckWindow = window;
            window.Show();
        }
        #endregion
    }
}
