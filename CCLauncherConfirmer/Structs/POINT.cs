﻿using System.Drawing;
using System.Runtime.InteropServices;

namespace CCLauncherConfirmer.Structs
{
    // https://stackoverflow.com/a/5577528
    /// <summary>
    /// Struct representing a point.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    struct POINT
    {
        public int X;
        public int Y;

        public static implicit operator Point(POINT point)
        {
            return new Point(point.X, point.Y);
        }
    }
}
