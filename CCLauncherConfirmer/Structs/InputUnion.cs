﻿using System.Runtime.InteropServices;

namespace CCLauncherConfirmer.Structs
{
    [StructLayout(LayoutKind.Explicit)]
    struct InputUnion
    {
        [FieldOffset(0)]
        internal MOUSEINPUT mi;
    }
}
