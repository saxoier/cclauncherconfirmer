﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace CCLauncherConfirmer.Wpf.Converters
{
    class ScaleConverter : IValueConverter
    {
        #region Public
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
            => value is double scale ? new ScaleTransform(scale, scale) : null;

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
            => throw new NotImplementedException();
        #endregion
    }
}
