﻿using CCLauncherConfirmer.Enums;
using System;
using System.Runtime.InteropServices;

namespace CCLauncherConfirmer.Structs
{
    [StructLayout(LayoutKind.Sequential)]
    struct MOUSEINPUT
    {
        internal int dx;
        internal int dy;
        internal int mouseData;
        internal MOUSEEVENTF dwFlags;
        internal uint time;
        internal UIntPtr dwExtraInfo;
    }
}
