﻿using CCLauncherConfirmer.Wpf.Fake.Resources;
using Saxoier.Utils.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Windows;
using System.Windows.Input;

namespace CCLauncherConfirmer.Wpf.Fake.ViewModels
{
    class AttendanceCheckViewModel : ViewModel
    {
        #region Properties
        private Random Rng { get; }
        private IList<Image> TestImages { get; }

        public ObservableCollection<PointViewModel> MouseClicks { get; }
        public Image SelectedTestImage
        {
            get => GetValue<Image>();
            set => SetValue(value);
        }
        #endregion

        #region Constructors
        public AttendanceCheckViewModel()
        {
            AddCursorPositionCommand = new RelayCommand<IInputElement>(AddCursorPosition);
            ClearMouseClicksCommand = new RelayCommand(ClearMouseClicks);
            SelectRandomImageCommand = new RelayCommand(SelectRandomImage);

            MouseClicks = new ObservableCollection<PointViewModel>();
            Rng = new Random();
            TestImages = new List<Image>()
            {
                TestFiles.test1608, TestFiles.test3705, TestFiles.test4272,
                TestFiles.test5612, TestFiles.test5993, TestFiles.test6500,
                TestFiles.test6815, TestFiles.test9182
            };

            SelectRandomImage();
        }
        #endregion

        #region Commands
        public ICommand<IInputElement> AddCursorPositionCommand { get; }
        public ICommand ClearMouseClicksCommand { get; }
        public ICommand SelectRandomImageCommand { get; }
        #endregion

        #region Commandactions
        private void AddCursorPosition(IInputElement inputElement)
        {
            var mouseClicks = MouseClicks;

            var m = (mouseClicks.Count, Mouse.GetPosition(inputElement));
            var vm = CreateViewModel<(int, System.Windows.Point), PointViewModel>(m);
            mouseClicks.Add(vm);
        }

        private void ClearMouseClicks()
        {
            MouseClicks.Clear();
        }

        private void SelectRandomImage()
        {
            var testImages = TestImages;
            SelectedTestImage = testImages[Rng.Next(0, testImages.Count)];
        }
        #endregion
    }
}
