﻿using Accord.Imaging;
using Accord.Imaging.Filters;
using CCLauncherConfirmer.Extensions;
using Saxoier.Utils;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;

namespace CCLauncherConfirmer
{
    public class ImageTester
    {
        #region Properties
        private List<(string, Bitmap)> Digits { get; }
        private List<(string, Bitmap)> PadDigits { get; }
        private Crop CropRectDigits { get; }
        private Crop CropRectPadDigits { get; }
        private ExhaustiveTemplateMatching TemplateMatcher { get; }

        public List<(string name, TemplateMatch match)> DigitMatches { get;  }
        public List<(string name, TemplateMatch match)> PadDigitMatches { get; }
        #endregion

        #region Constructor
        public ImageTester()
        {
            Digits = ConvertTo24bppRgb(new[]
            {
                ("0", Numbers.number0), ("1", Numbers.number1),
                ("2", Numbers.number2), ("3", Numbers.number3),
                ("4", Numbers.number4), ("5", Numbers.number5),
                ("6", Numbers.number6), ("7", Numbers.number7),
                ("8", Numbers.number8), ("9", Numbers.number9)
            }).ToList();
            PadDigits = ConvertTo24bppRgb(new[]
            {
                ("0", Numbers.pad0), ("1", Numbers.pad1),
                ("2", Numbers.pad2), ("3", Numbers.pad3),
                ("4", Numbers.pad4), ("5", Numbers.pad5),
                ("6", Numbers.pad6), ("7", Numbers.pad7),
                ("8", Numbers.pad8), ("9", Numbers.pad9)
            }).ToList();
            CropRectDigits = new Crop(new Rectangle(new Point(32, 122), new Size(42, 13)));
            CropRectPadDigits = new Crop(new Rectangle(new Point(139, 187), new Size(77, 112)));
            TemplateMatcher = new ExhaustiveTemplateMatching(0.95f);
            DigitMatches = new List<(string, TemplateMatch)>();
            PadDigitMatches = new List<(string, TemplateMatch)>();
        }
        #endregion

        #region Private
        private IEnumerable<(string, Bitmap)> ConvertTo24bppRgb(IEnumerable<(string, Bitmap)> bitmaps)
        {
            foreach (var (name, bitmap) in bitmaps)
            {
                yield return (name, bitmap.ConvertToFormat(PixelFormat.Format24bppRgb));
            }
        }

        private TemplateMatch AddOffset(TemplateMatch match, Rectangle templateOffset)
        {
            var x = match.Rectangle.X + templateOffset.X;
            var y = match.Rectangle.Y + templateOffset.Y;
            var width = match.Rectangle.Width;
            var height = match.Rectangle.Height;

            return new TemplateMatch(new Rectangle(x, y, width, height), match.Similarity);
        }

        private IEnumerable<(string, TemplateMatch)> GetRects(Bitmap image, IEnumerable<(string, Bitmap)> templates, Rectangle templateOffset)
        {
            foreach (var (templateName, template) in templates)
            {
                foreach (var match in TemplateMatcher.ProcessImage(image, template))
                {
                    yield return (templateName, AddOffset(match, templateOffset));
                }
            }
        }
        #endregion

        #region Public
        public void EvaluateRects(System.Drawing.Image image)
        {
            using (var bitmap = image.ConvertToFormat(PixelFormat.Format24bppRgb))
            using (var bitmapDigits = CropRectDigits.Apply(bitmap))
            using (var bitmapPadDigits = CropRectPadDigits.Apply(bitmap))
            {
                DigitMatches.Replace(GetRects(bitmapDigits, Digits, CropRectDigits.Rectangle));
                PadDigitMatches.Replace(GetRects(bitmapPadDigits, PadDigits, CropRectPadDigits.Rectangle));
            }
        }
        #endregion
    }
}
